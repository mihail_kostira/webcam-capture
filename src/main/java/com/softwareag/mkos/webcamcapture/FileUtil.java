package com.softwareag.mkos.webcamcapture;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;

public class FileUtil {
	
	 public  static void copyFileUsingRandomAccessFile(File source, File dest) throws IOException {

	        try(
	            //InputStream is = new FileInputStream(source);
	        	RandomAccessFile src = new RandomAccessFile(source, "rw");
	        	FileLock srcLock = src.getChannel().lock();
		        RandomAccessFile raf = new RandomAccessFile(dest, "rw");
	        	FileLock destLock = raf.getChannel().lock();
	            ) {
	        	 byte[] buffer = new byte[1024];
	            int length;
	            while ((length = src.read(buffer)) > 0) {
	                raf.write(buffer, 0, length);
	            }
	        } catch (Exception e) {
				System.err.println(e);
	        } finally {
			}
	    }

	public static void copyFileUsingChannel(File source, File dest) throws IOException {
		FileLock srcLock = null;
		FileLock destLock = null;
		FileChannel sourceChannel = null;
		FileChannel destChannel = null;
		try {
			sourceChannel = new FileInputStream(source).getChannel();
			srcLock = sourceChannel.lock();

			destChannel = new FileOutputStream(dest).getChannel();
			destLock = destChannel.lock();

			destChannel.transferFrom(sourceChannel, 0, sourceChannel.size());
		} catch (Exception e) {
			System.err.println(e);
		} finally {
			sourceChannel.close();
			srcLock.release();
			destChannel.close();
			destLock.release();
		}
	}
}

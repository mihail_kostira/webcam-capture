package com.softwareag.mkos.webcamcapture;

import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.Deque;
import java.util.LinkedList;

import javax.imageio.ImageIO;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import com.github.sarxos.webcam.Webcam;

@Component
public class CaptureService {
	
	@Value("${camera.viewWidth}")
	private int camWidth;
	
	@Value("${camera.viewHeight}")
	private int camHeight;
	
	@Value("${camera.captureIntervalMillis}")
	private int captureIntervalMillis;
	
	@Value("${outputFolder}")
	private String outputFolderName;
	
	@Value("${maxFilesToKeep}")
	private int maxFilesToKeep;
	
	private Deque<Path> storedFiles;
	
	public CaptureService() {
		storedFiles = new LinkedList<Path>();
	}
	
	@EventListener(ApplicationReadyEvent.class)
	public void onApplicationReady() throws InterruptedException, IOException {
		new Thread(() -> {
			run();
		}).start();
	}
	
	
	public void run() {
		// get default webcam and open it
		System.out.println("Attempting to open camera...");
		Webcam webcam = Webcam.getDefault();
		webcam.setViewSize(new Dimension(camWidth, camHeight));
		webcam.open();
		
		Path outputFolder = Paths.get(outputFolderName);
		
		System.out.println(String.format("Starting webcam capture every %d millis at resolution %dx%d. Output folder is %s. Last %d captures will be kept",
				captureIntervalMillis, camWidth, camHeight, outputFolderName, maxFilesToKeep));
		
		if (!outputFolder.toFile().exists()) {
			throw new IllegalStateException("output folder does not exist: " + outputFolder.toFile().getAbsolutePath());
		}
		
		while (true) {
			// get image
			BufferedImage image = webcam.getImage();

			// save image to PNG file
			String fileName =  "cap" + String.valueOf(new Date().getTime()) + ".png";
			
			File file = new File(fileName);
			
			try {
				ImageIO.write(image, "PNG", file);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			// now move it
			File output = outputFolder.resolve(fileName).toFile();
			try {
				FileUtil.copyFileUsingRandomAccessFile(file, output);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			file.delete();
			
			try {
				Thread.sleep(captureIntervalMillis);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			storedFiles.add(output.toPath());
			if (storedFiles.size() > maxFilesToKeep) {
				Path removed = storedFiles.remove();
				boolean deleted = removed.toFile().delete();
			}
		}
	}
}
